// -- ****************************************************************/
// -- ****************************************************************/
// --                                                 ,,             */
// --          `7MMM.     ,MMF'         `7MMF'        db             */
// --            MMMb    dPMM             MM                         */
// --            M YM   ,M MM   .gP"Ya    MM        `7MM             */
// --            M  Mb  M' MM  ,M'   Yb   MM          MM             */
// --            M  YM.P'  MM  8M""""""   MM      ,   MM             */
// --            M  `YM'   MM  YM.    ,   MM     ,M   MM             */
// --          .JML. `'  .JMML. `Mbmmd' .JMMmmmmMMM .JMML.           */
// -- ****************************************************************/
// -- ****************************************************************/
/* --                          recomendador.go                       */
/* --                                                                */
/* --  Descripcion: Contiene todo el codigo de la logica del         */
/* --               ejercicio.                                       */
/* --                                                                */
/* --   @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>      */
/* --                                                                */
/* --  © 2022 - Mercado Libre - Desafio Tecnico SOLID                */
/* -- ****************************************************************/

package recomendador

import (
	"fmt"

	cc "gitlab.com/rodrigoghm/gosrp_ejercicio/class/clientes"
	"gitlab.com/rodrigoghm/gosrp_ejercicio/class/model"
	pp "gitlab.com/rodrigoghm/gosrp_ejercicio/class/peliculas"
	"gitlab.com/rodrigoghm/gosrp_ejercicio/utils"
)

func GetRecomendaciones(c cc.Clientes) []pp.Peliculas {
	var recomendadas []pp.Peliculas
	var directores []string

	for _, m := range c.GetFavoritas() {
		flg := utils.Contains(directores, m.GetDirector())
		if flg == false {
			recomendadas = append(recomendadas, model.PelisPorDirector(m.GetDirector())...)
			directores = append(directores, m.GetDirector())
		}
	}

	return recomendadas
}

func GetRecomendacionesCSV(c cc.Clientes) string {
	P := GetRecomendaciones(c)
	var cad string

	for _, peli := range P {
		cad = cad + fmt.Sprintf("%s, %s, %s\n", peli.GetTitulo(), peli.GetGenero(), peli.GetDirector())
	}

	return cad
}
